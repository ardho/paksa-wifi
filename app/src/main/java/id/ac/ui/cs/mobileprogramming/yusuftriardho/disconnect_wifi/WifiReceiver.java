package id.ac.ui.cs.mobileprogramming.yusuftriardho.disconnect_wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class WifiReceiver extends BroadcastReceiver {

    MainActivity main = null;

    void setMainActivityHandler(MainActivity main){
        this.main = main;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(">>","received");
        main.setWifi();
    }


}
